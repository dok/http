Код к серии статей про HTTP протокол:

1. [Http в java. Часть первая - TCP.](http://www.dokwork.ru/2012/02/http-java-tcp.html)
2. [Http в Java. Часть вторая - HTTP.](http://www.dokwork.ru/2012/02/http-java-http.html)
3. [Http в Java. Часть 2.5 - Простой web server.](http://www.dokwork.ru/2012/06/http-java-25-web-server.html)

И статью про технологию Ajax:

[Коротко о том, что такое Ajax](http://www.dokwork.ru/2012/07/ajax.html)